package RCP05_TP1;

public abstract class Minion {

	public String nom;
	public int bourse;
	public int vie;

	public Minion(String nom){
		this.nom=nom; 
		bourse=0;
		vie=6+(int)(Math.random()*((10-6)+1));
	}
	public int getVie(){
		return vie;
	}

	public void perdreVie(int v){
		if(vie>0){
			vie=vie-v;
		}
		
	}
}